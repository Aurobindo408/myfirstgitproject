package com.example.attendancesheet;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Response;

public class upload extends AppCompatActivity {



    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    ArrayList<String> data_list = new ArrayList<>();
    ArrayList<String> data_list1 = new ArrayList<>();
    private static RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_upload );


        data_list.add( "Mango" );
        data_list.add( "Apple" );
        data_list.add( "Orange" );
        data_list.add( "Watermelon" );
        data_list.add( "Pinapple" );


        data_list1.add( "https://media.istockphoto.com/photos/mango-isolated-on-white-background-picture-id911274308?k=6&m=" +
                "911274308&s=612x612&w=0&h=WjFtDBD8zVP6HP7fpYvrPvZyNRU_qkRRGCymS3SX5GQ=" );
        data_list1.add( "https://cdn1.vectorstock.com/i/1000x1000/74/95/set-of-red-and-green-apple-fruits-with-cut-and-vector-1637495.jpg" );
        data_list1.add( "https://cmkt-image-prd.freetls.fastly.net/0.1.0/ps/1884251/910/715/m2/fpnw/wm1/fji8rqbgmxffy7lurxmk2t" +
                "pad6mjx40zhswcp9ai4sr9xyotwbxptnc2addkwcpl-.jpg?1478955945&s=57270ec0094eb2a913f2cace8929e925" );
        data_list1.add( "https://www.greatbasket.in/wp-content/uploads/2018/06/Watermelon.jpg" );
        data_list1.add( "http://pngimg.com/uploads/pineapple/pineapple_PNG2759.png" );


        recyclerView = findViewById( R.id.resign );
        recyclerView.setHasFixedSize( true );
        layoutManager = new LinearLayoutManager( this );
        recyclerView.setLayoutManager( layoutManager );
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration( upload.this, R.dimen.item_offset );
        recyclerView.addItemDecoration( itemDecoration );
        recyclerView.setLayoutManager( new LinearLayoutManager( this, LinearLayoutManager.HORIZONTAL, true ) );


        recyclerView.setItemAnimator( new DefaultItemAnimator() );


        recyclerAdapter = new RecyclerAdapter( upload.this, data_list, data_list1 );
        recyclerView.setAdapter( recyclerAdapter );



    }

}
