package com.example.attendancesheet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button sign;
    String ed1,ed2;
    TextView p, q;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        toolbar = findViewById( R.id.toolbar );
        p = findViewById( R.id.name1 );
        q = findViewById( R.id.name2 );
        sign = findViewById( R.id.btn );

        sign.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed1 = p.getText().toString();
                ed2 = q.getText().toString();



                Intent intent = new Intent( MainActivity.this, Electronicsignature.class );
                intent.putExtra("test1","test1");
                startActivity( intent );


            }
        } );
    }
}
