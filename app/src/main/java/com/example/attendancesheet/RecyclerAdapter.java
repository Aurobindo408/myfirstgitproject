package com.example.attendancesheet;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ListViewHolder> {


    ArrayList<String >arrayList;
    ArrayList<String >arrayList1;
    Context context;

    public RecyclerAdapter(Context context,ArrayList<String >arrayList, ArrayList<String >arrayList1) {
        this.context = context;
        this.arrayList = arrayList;
        this.arrayList1=arrayList1;
    }


    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.myadapter, null, false);
        itemView.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ListViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, final int position) {
        holder.fname.setText(arrayList.get(position));
        Picasso.get().load(arrayList1.get(position)).into(holder.image);

        holder.fname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context,"" +arrayList.get(position)+""+arrayList1.get(position), Toast.LENGTH_SHORT).show();
            }
        });



    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    class ListViewHolder extends RecyclerView.ViewHolder {
        TextView fname;
        ImageView image;

        ListViewHolder(View view) {
            super(view);
            fname = view.findViewById(R.id.fname);
            image=view.findViewById(R.id.image);
        }
    }
}